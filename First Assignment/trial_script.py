ath 	= "alt.atheism/"
graph 	= "comp.graphics/"

# Part 1

import os

# Prefix is a variable that contains the directory where each newsgroup is located
# Notice that a double \\ has to be used when \ is desired within a string
prefix = "20news-18828/"        
directories = os.listdir(prefix)
print(len(directories))
print(directories)

#Part 2

#fileNames = os.listdir(prefix+ath)
#print(fileNames[:5])

#fileName = prefix+ath+"49960"
#print "The name of the file that contains the first message is: {}".format(fileName)
#print "================================"

#Part 3

## Now, we open the file for reading
#myFile = open(fileName, "r")
## and it's read whole.
#firstMessage = myFile.read()
#myFile.close() # The file is no longer needed, so it is closed
#print(firstMessage)

#Part 4

#fileNames = os.listdir(prefix+graph)
#fileName = prefix+graph+fileNames[0]
#print "The name of the file that contains the first message is: {}".format(fileName)
#print "================================"

## Now, we open the file for reading
#myFile = open(fileName, "r")
## and it's read whole.
#firstMessage = myFile.read()
#myFile.close() # The file is no longer needed, so it is closed
#print(firstMessage)

#Part 5

def readMessage(fileName):
    # Now, we open the file for reading
    myFile = open(fileName, "r")
    # and it's read whole.
    message = myFile.read()
    myFile.close() # The file is no longer needed, so it is closed    
    return(message)

#Part 6

# Files contains the list of files (messages) of newsgroup alt.atheism
files = os.listdir(prefix+ath)

# Let's read the first two messages

message1_aa = readMessage(prefix+ath+files[0])
message2_aa = readMessage(prefix+ath+files[1])

# Let's put them into a list:

messages_aa = [message1_aa, message2_aa]

print "We have read {} messages".format(len(messages_aa))
print "=================================================="
print "The first message starts with these words:\n{}".format(messages_aa[0][0:200])
print "=================================================="
print "And the second message starts with these words:\n{}".format(messages_aa[1][0:200])

#Part 7

message3_aa = readMessage(prefix+ath+files[2])
messages_aa.append(message3_aa)
print(len(messages_aa))

#Part 8

files = os.listdir(prefix+ath)

messages_aa = [] # Initially, we have no read any message (empty list)
# Let's read 5 messages (but it could be any number)
for myFile in files[:5]:
    messages_aa.append(readMessage(prefix+ath+myFile))
print "We have read {} messages".format(len(messages_aa))

#Part 9

files = os.listdir(prefix+graph)

messages_cg = [] # Initially, we have no read any message (empty list)
# Let's read 5 messages (but it could be any number)
for myFile in files[:5]:
    messages_cg.append(readMessage(prefix+graph+myFile))
print "We have read {} messages from {}".format(len(messages_cg), graph)

#Part 10

myMessage =  "Somewhere in la Mancha, in a place whose name I do not care to remember, a gentleman lived not long ago, one of those who has a lance and ancient shield on a shelf and keeps a skinny nag and a greyhound for racing." 
print(myMessage)
print("")
words = myMessage.split()
print "The list of words is:\n{}".format(words)

#Part 11

myMessage =  "Somewhere in la Mancha, in a place whose name I do not care to remember, a gentleman lived not long ago, one of those who has a lance and ancient shield on a shelf and keeps a skinny nag and a greyhound for racing." 
print(myMessage)
print("")
myMessage = myMessage.replace(",", " ")
myMessage = myMessage.replace(".", " ")

words = myMessage.split()
print "The list of words is:\n{}".format(words)

#Part 12

# First, let's make a copy of the original list "messages_aa", 
# so that messages_aa is not modified and we can use it later unchanged

import copy
messages_aa_copy = copy.deepcopy(messages_aa)

# Now, we will modify messages_aa_copy and remove , and . from them
# Let's remove "," and "." from the first message
messages_aa_copy[0] = messages_aa_copy[0].replace(",", " ")
messages_aa_copy[0] = messages_aa_copy[0].replace(".", " ")
messages_aa_copy[0] = messages_aa_copy[0].split()

# Same for the second message
messages_aa_copy[1] = messages_aa_copy[1].replace(",", " ")
messages_aa_copy[1] = messages_aa_copy[1].replace(".", " ")
messages_aa_copy[1] = messages_aa_copy[1].split()

# Same for the third message
messages_aa_copy[2] = messages_aa_copy[2].replace(",", " ")
messages_aa_copy[2] = messages_aa_copy[2].replace(".", " ")
messages_aa_copy[2] = messages_aa_copy[2].split()


print "=================================================="
print "We can check what happenned to the first 20 words of the first message:"
print(messages_aa_copy[0][0:50]) 
print "=================================================="

#Part 13

# len(messages_aa) is the number of elements in the list (i.e. the number of messages, 
# 3 in this case)
for messageNumber in range(0,(len(messages_aa))):
    messages_aa[messageNumber] = messages_aa[messageNumber].replace(",", " ")
    messages_aa[messageNumber] = messages_aa[messageNumber].replace(".", " ")
    messages_aa[messageNumber] = messages_aa[messageNumber].split()
    
    
print "=================================================="
print "We can check what happened to the first 50 words of the first message:"
print(messages_aa[0][0:50]) 
print "=================================================="

#Part 14

def createDictionary(words):
    d = {}
    for word in words:
        if word in d:
            d[word] = d[word] + 1
        else:
            d[word] = 1
    return(d)

#Part 15

myMessage =  "Somewhere in la Mancha, in a place whose name I do not care to remember, a gentleman lived not long ago, one of those who has a lance and ancient shield on a shelf and keeps a skinny nag and a greyhound for racing." 
print(myMessage)
print("")
myMessage = myMessage.replace(",", " ")
myMessage = myMessage.replace(".", " ")
words = myMessage.split()

# Here, we create a dictionary with the count of words
myDictionary = createDictionary(words)
print(myDictionary)

#Part 16

for word in myDictionary:
    myDictionary[word] = 100*myDictionary[word]/len(myDictionary)
    
print(myDictionary) 


myDictionary = createDictionary(messages_aa[0])

for word in myDictionary:
    myDictionary[word] = 100*myDictionary[word]/len(myDictionary)
    
print(myDictionary)






























