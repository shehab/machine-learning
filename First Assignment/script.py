import os

# Prefix is a variable that contains the directory where each newsgroup is located
# Notice that a double \\ has to be used when \ is desired within a string
prefix = "20news-18828/"        
file_names_graph = prefix + "alt.atheism/"
file_names_athei = prefix + "comp.graphics/"

def readMessage(fileName):
    # Now, we open the file for reading
    myFile = open(fileName, "r")
    # and it's read whole.
    message = myFile.read()
    myFile.close() # The file is no longer needed, so it is closed    
    return(message)

files = os.listdir(file_names_athei)

# Let's read the first two messages

message1_aa = readMessage(file_names_athei+files[0])
message2_aa = readMessage(file_names_athei+files[1])

# Let's put them into a list:

messages_aa = [message1_aa, message2_aa]

print "We have read {} messages".format(len(messages_aa))
print "=================================================="
print "The first message starts with these words:\n{}".format(messages_aa[0][0:200])
print "=================================================="
print "And the second message starts with these words:\n{}".format(messages_aa[1][0:200])

message3_aa = readMessage(file_names_athei+files[2])
messages_aa.append(message3_aa)
print(len(messages_aa))